;; init.el ---  Personal Emacs configration entry point. -*- lexical-binding: t -*-
;;
;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

(load-file (expand-file-name "_variables.el" user-emacs-directory))

(let ((local-conf-file (expand-file-name "local-settings.el" user-emacs-directory)))
  (when (file-readable-p local-conf-file)
    (load-file local-conf-file)))

(setq column-number-mode t
      inhibit-splash-screen t)

(let ((default-directory (expand-file-name "conf" user-emacs-directory)))
  (normal-top-level-add-subdirs-to-load-path))

(require 'install-packages)
(load-theme 'flatui-dark t)

(global-undo-tree-mode)

(require 'better-defaults)
(require 'generic-conf)
(require 'languages)

(let ((local-override-file (expand-file-name "local-overrides.el" user-emacs-directory)))
  (when (file-readable-p local-override-file)
    (load-file local-override-file)))

;;; init.el ends here
