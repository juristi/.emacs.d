;;; _lsp_tuning.el --- Language server tuning configuration

;;
;; This file is not part of GNU Emacs

;;; Commentary:
;; Language server performance tunings as described in
;; https://emacs-lsp.github.io/lsp-mode/page/performance/

;;; Code:
(setq gc-cons-threshold 1600000)  ;; 1,6Mb
(setq read-process-output-max (* 1024 1024)) ;; 1MB

(provide '_lsp_tuning)
;;; _lsp_tuning.el ends here
