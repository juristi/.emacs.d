;;; generic-conf.el --- A miscellanious compilation of snippets and tweaks.

;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:
(require 'use-package)
(setq use-package-always-defer t
      use-package-always-ensure t)

(require '_agenda)
(require '_lsp_tuning)
(require '_treesitter)
(require '_wcheck)
(require '_org)

;; Run plantuml in executable mode by default
(setq plantuml-executable-path "/usr/bin/plantuml"
      plantuml-default-exec-mode 'executable
      org-plantuml-jar-path "/usr/share/java/plantuml/plantuml.jar")
;; Enable plantuml-mode for PlantUML files
(add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
;, Enable plantuml processing in org mode
(org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))

;; By http://endlessparentheses.com/implementing-comment-line.html
(defun comment-or-uncomment-line-or-region (n)
  "Comment or uncomment current line and leave point after it.
With positive prefix, apply to N lines including current one.
With negative prefix, apply to -N lines above.
If region is active, apply to active region instead."
  (interactive "p")
  (if (use-region-p)
      (comment-or-uncomment-region
       (region-beginning) (region-end))
    (let ((range
           (list (line-beginning-position)
                 (goto-char (line-end-position n)))))
      (comment-or-uncomment-region
       (apply #'min range)
       (apply #'max range)))
    (forward-line 1)
    (back-to-indentation)))

(define-key prog-mode-map (kbd "C-;") 'comment-or-uncomment-line-or-region)

(defalias 'yes-or-no-p 'y-or-n-p)

(global-set-key (kbd "C-c a") #'org-agenda)

(global-set-key (kbd "C-x g") #'magit-status)
(global-git-gutter-mode +1)

(use-package forge
  :after magit)

(use-package projectile
  :init
  (projectile-mode +1)
  :custom
  (projectile-ignored-projects '("~/"))
  :bind (:map projectile-mode-map ("C-c p" . projectile-command-map)))

(require 'yasnippet)
(yas-global-mode 1)
(global-set-key (kbd "<C-tab>") #'ivy-yasnippet)

(require '_ivy)
(require 'editorconfig)
(editorconfig-mode 1)

;; Unclutter modeline from unneeded package info
(require 'diminish)
(eval-after-load "editorconfig" '(diminish 'editorconfig-mode))
(eval-after-load "git-gutter" '(diminish 'git-gutter-mode))
(eval-after-load "ivy" '(diminish 'ivy-mode))
(eval-after-load "undo-tree" '(diminish 'undo-tree-mode))
(eval-after-load "prettier-js" '(diminish 'prettier-js-mode))

;; Prevent Org mode from indenting after newline
(add-hook 'org-mode-hook (lambda () (electric-indent-local-mode -1)))

;; Update inline images after source updates
(add-hook 'org-babel-after-execute-hook
          (lambda ()
            (when org-inline-image-overlays
              (org-redisplay-inline-images))))

(setq ediff-split-window-function #'split-window-horizontally)

(provide 'generic-conf)

;;; generic-conf.el ends here

