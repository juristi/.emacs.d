;;; _org.el --- Additional org-mode configuration

;;
;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:
(use-package org-roam
  :ensure t
  ;; :init
  ;; (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory (file-truename juristi/org-roam-directory))
  (org-roam-dailies-directory "journals/")
  (org-roam-capture-templates
   '(("d" "default" plain "%?"
      :target (file+head "pages/${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)))
  ;; ensure your org-roam daily template follows the journal settings in Logseq
  ;;    :journal/page-title-format "yyyy-MM-dd"
  ;;    :journal/file-name-format "yyyy_MM_dd"
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %?"
      :target (file+head "%<%Y_%m_%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  (org-roam-node-display-template (concat "${title:50} " (propertize "${tags}" 'face 'org-tag)))
  ;; exclude all syncthing folders and anything under logseq/ from being indexed by org-roam
  (org-roam-file-exclude-regexp "\\.st[^/]*\\|logseq/.*$")
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  ;; (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  ;;(setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags}" 'face 'org-tag)))
  )

(provide '_org)
;;; _org.el ends here
