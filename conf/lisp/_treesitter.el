;;; _lsp_tuning.el --- Language server tuning configuration
;;
;; This file is not part of GNU Emacs

;;; Commentary:
;; Language server performance tunings as described in
;; https://emacs-lsp.github.io/lsp-mode/page/performance/

;;; Code:

(use-package treesit
  :ensure nil
  :config
  (setq treesit-language-source-alist
        '(
          (c "https://github.com/tree-sitter/tree-sitter-c" "master" "src" nil nil)
          (c++ "https://github.com/tree-sitter/tree-sitter-cpp" "master" "src" nil nil)
          (css "https://github.com/tree-sitter/tree-sitter-css" "master" "src" nil nil)
          (elixir "https://github.com/elixir-lang/tree-sitter-elixir" "main" "src" nil nil)
          (elm "https://github.com/elm-tooling/tree-sitter-elm" "main" "src" nil nil)
          (go "https://github.com/tree-sitter/tree-sitter-go" "master" "src" nil nil)
          (gomod "https://github.com/camdencheek/tree-sitter-go-mod" "main" "src" nil nil)
          (haskell "https://github.com/tree-sitter/tree-sitter-haskell" "master" "src" nil nil)
          (html "https://github.com/tree-sitter/tree-sitter-html" "master" "src" nil nil)
          (java "https://github.com/tree-sitter/tree-sitter-java" "master" "src" nil nil)
          (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src" nil nil)
          (json "https://github.com/tree-sitter/tree-sitter-json" "master" "src" nil nil)
          (python "https://github.com/tree-sitter/tree-sitter-python" "master" "src" nil nil)
          (rust "https://github.com/tree-sitter/tree-sitter-rust" "master" "src" nil nil)
          (scala "https://github.com/tree-sitter/tree-sitter-scala" "master" "src" nil nil)
          (toml "https://github.com/tree-sitter/tree-sitter-toml" "master" "src" nil nil)
          (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src" nil nil)
          (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src" nil nil)
          (yaml "https://github.com/ikatyang/tree-sitter-yaml" "master" "src" nil nil))))

(provide '_treesitter)
;;; _treesitter.el ends here
