;;; _wcheck.el --- WCheck configuration

;;
;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:

(setq wcheck-language-data
      '(("British English"
         (program . "/usr/bin/enchant-2")
         (args "-l" "-d" "en_GB")
         (action-program . "/usr/bin/enchant-2")
         (action-args "-a" "-d" "en_GB")
         (action-parser . wcheck-parser-ispell-suggestions))
        ("Finnish"
         (program . "/usr/bin/enchant-2")
         (args "-l" "-d" "fi")
         (action-program . "/usr/bin/enchant-2")
         (action-args "-a" "-d" "fi")
         (action-parser . wcheck-parser-ispell-suggestions))))

;; (message "WCHECK-LANGUAGE: %s" wcheck-language)
(customize-set-value 'wcheck-language (car (car wcheck-language-data)))
;; (message "WCHECK-LANGUAGE: %s" wcheck-language)

(require 'wcheck-mode)
(autoload 'wcheck-mode "wcheck-mode"
  "Toggle wcheck-mode." t)
(autoload 'wcheck-change-language "wcheck-mode"
  "Switch wcheck-mode languages." t)
(autoload 'wcheck-actions "wcheck-mode"
  "Open actions menu." t)
(autoload 'wcheck-jump-forward "wcheck-mode"
  "Move point forward to next marked text area." t)
(autoload 'wcheck-jump-backward "wcheck-mode"
  "Move point backward to previous marked text area." t)

(add-hook 'text-mode-hook #'wcheck-mode)

(define-key wcheck-mode-map (kbd "C-c s l") #'wcheck-change-language)
(define-key wcheck-mode-map (kbd "C-c s c") #'wcheck-actions)
(define-key wcheck-mode-map (kbd "C-c s f") #'wcheck-jump-forward)
(define-key wcheck-mode-map (kbd "C-c s b") #'wcheck-jump-backward)

(provide '_wcheck)
;;; _wcheck.el ends here
