;;; _agenda.el --- Org agenda configuration -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:

;; Set up Org mode
(require 'org)
(require 'suomalainen-kalenteri)

;; Basic agenda configuration
(setq calendar-week-start-day 1)
(setq org-refile-allow-creating-parent-nodes t)
(setq org-refile-use-outline-path 'file)
(setq org-outline-path-complete-in-steps nil)

(setq calendar-holidays 'suomalainen-kalenteri)

;; Agenda file locations and templates
(setq org-directory juristi/org-directory)
(setq org-default-notes-file (expand-file-name "notes.org" org-directory))
(define-key global-map (kbd "C-c c") 'org-capture)

(setq org-todo-keywords '((sequence "TODO(t)"
                                    "NEXT(n)"
                                    "WAITING(w)"
                                    "|"
                                    "DONE(d!)"
                                    "CANCELLED(c!)")))

(setq org-agenda-files juristi/org-agenda-files)
(setq org-refile-targets juristi/org-refile-targets)

(setq org-capture-templates
      `(("t" "Todo [inbox]" entry
         (file+headline ,(expand-file-name "inbox.org" juristi/org-agenda-directory) "Tasks")
         "* TODO %?\n%i")
        ("T" "Todo @work [inbox-work]" entry
         (file+headline ,(expand-file-name "inbox-work.org" juristi/org-agenda-directory) "Tasks")
         "* TODO %?\n%i")
        ("S" "Timed" entry
         (file+headline ,(expand-file-name "scheduled.org" juristi/org-agenda-directory) "Scheduler")
         "* %i%? \n %U")))

;; Set up org-caldav
(unless (file-exists-p juristi/org-caldav-state-directory)
  (with-file-modes #o700 (make-directory juristi/org-caldav-state-directory t)))

(setq org-caldav-url juristi/org-caldav-url)
(setq org-caldav-save-directory juristi/org-caldav-state-directory)
(setq org-caldav-calendars juristi/org-caldav-calendars)

(let ((agenda-prefix-format " %i %-20b%?-12t% s "))
  (customize-set-value 'org-agenda-custom-commands
                       `(("w" "Työnäkymä"
                          ((agenda "" ((org-agenda-span 1)))
                           (tags-todo "+@työ/NEXT" ((org-agenda-overriding-header "Työtehtävät")
                                                    (org-agenda-prefix-format ,agenda-prefix-format)))
                           (tags-todo "-@työ/NEXT" ((org-agenda-overriding-header "Muut tehtävät")
                                                    (org-agenda-prefix-format ,agenda-prefix-format)))
                           (tags-todo "+TODO=\"WAITING\"" ((org-agenda-overriding-header "Odottavat")
                                                           (org-agenda-prefix-format ,agenda-prefix-format)))
                           (tags-todo "+TODO=\"TODO\"" ((org-agenda-overriding-header "Jono")
                                                        (org-agenda-prefix-format ,agenda-prefix-format))))
                          ((org-agenda-compact-blocks t)))
                         ("h" "Kotinäkymä"
                          ((agenda "" ((org-agenda-span 1)))
                           (tags-todo "-@työ/NEXT" ((org-agenda-overriding-header "Tehtävät")))
                           (tags-todo "+TODO=\"WAITING\"" ((org-agenda-overriding-header "Odottavat")))
                           (tags-todo "+TODO=\"TODO\"" ((org-agenda-overriding-header "Jono")))
                           (tags-todo "+@työ/NEXT" ((org-agenda-overriding-header "Työtehtävät"))))
                          ((org-agenda-compact-blocks t))))))

(define-key global-map (kbd "C-c C-x C-o") #'org-clock-out)
(define-key global-map (kbd "C-c C-x C-x") #'org-clock-in-last)

(provide '_agenda)
;;; _agenda.el ends here
