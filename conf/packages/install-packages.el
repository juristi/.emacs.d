;; install-packages.el ---  Verify that all wanted packages are installed

;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:

(require 'package)

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar my-wanted-packages
  '(better-defaults
    company
    company-go
    counsel
    counsel-projectile
    counsel-tramp
    diminish
    dockerfile-mode
    editorconfig
    elixir-mode
    elm-mode
    elpy
    flatui-dark-theme
    flycheck
    git-gutter
    go-mode
    inf-elixir
    ivy
    ivy-yasnippet
    js2-mode
    json-mode
    lsp-metals
    lsp-mode
    lsp-ui
    magit
    paredit
    projectile
    sly
    suomalainen-kalenteri
    undo-tree
    use-package
    wcheck-mode
    web-mode
    yaml-mode
    yasnippet
    yasnippet-snippets)
  "Packages that should be installed after start-up.")

(dolist (pkg my-wanted-packages)
  (unless (package-installed-p pkg)
    (package-install pkg)))

(provide 'install-packages)

;;; install-packages.el ends here
