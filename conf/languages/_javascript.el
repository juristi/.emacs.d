;; _javascript.el --- Javascript specific packages and configurations -*- lexical-binding: t; -*-

;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:




;; Tuned configuration for Projectile NPM project type
(require 'projectile)
(let ((npm-executable (or (executable-find "pnpm")
                          (executable-find "npm"))))
  (when npm-executable
    (projectile-register-project-type 'npm '("package.json")
                                      :configure (format "%s install" npm-executable)
                                      :test (format "%s test" npm-executable)
                                      :run (format "%s start" npm-executable)
                                      :test-prefix "test_")))

(when (executable-find "yarn")
  (projectile-register-project-type 'yarn '("package.json" "yarn.lock")
                                    :configure "yarn install"
                                    :test "yarn run test"
                                    :run "yarn run start"
                                    :test-suffix ".test"))

(defun custom/flycheck-with-local-eslint ()
  "Use the project local eslint from npm_modules if it exists."
  (let ((eslint-path (custom/find-local-eslint)))
    (when eslint-path
      (make-variable-buffer-local 'flycheck-javascript-eslint-executable)
      (setq flycheck-javascript-eslint-executable eslint-path)))
  (when (flycheck-valid-checker-p 'javascript-eslint)
    (make-variable-buffer-local 'lsp-diagnostics-provider)
    (setq lsp-diagnostics-provider :none)
    (flycheck-select-checker 'javascript-eslint))
  )

(defun custom/setup-javascript-mode ()
  "Setup javascript specific configurations inside file suffix guard."
  (unless (string-suffix-p ".json" buffer-file-name)
    (prettier-mode))
  (when (string-suffix-p ".js" buffer-file-name)
    (js2-minor-mode +1)
    (lsp)))

(use-package js-ts-mode
  :ensure nil
  :hook ((js-ts-mode . custom/setup-javascript-mode))
  :mode (("\\.js\\'" . js-ts-mode)
         ("\\.cjs\\'" . js-ts-mode)
         ("\\.mjs\\'" . js-ts-mode))
  :diminish
  :bind (("M-." . xref-find-definitions))
  :custom
  (js-indent-level 2)
  ;; (js2-mode-show-parse-errors nil)
  ;; (js2-mode-show-strict-warnings nil)
  )

(provide '_javascript)

;;; _javascript.el ends here
