;;; _elixir.el --- Elixir/Erlang specific configurations

;; This file is not part of GNU Emacs
;;; Commentary:
;;; Code:

(require 'lsp)

(defun custom/setup-elixir-mode ()
  "Setup elixir specific buffer configurations."
  (add-hook 'before-save-hook #'lsp-format-buffer nil t)
  )

(use-package elixir-mode
  :hook ((elixir-mode . lsp)
         (elixir-mode . custom/setup-elixir-mode)))

(use-package inf-elixir
  :bind (:map elixir-mode-map
              ("C-c C-z" . 'inf-elixir-project)
              ("C-c C-c" . 'inf-elixir-send-buffer)
              ("C-c C-y e" . 'inf-elixir-send-line)
              ("C-c C-y r" . 'inf-elixir-send-region))
)

(provide '_elixir)
;;; _elixir.el ends here
