;;; _go.el --- Go lang specific configurations

;; This file is not part of GNU Emacs
;;; Commentary:
;;; Code:


(require 'go-mode)
(require 'go-ts-mode)

(defun setup-go-mode ()
  "Set up Go language specific hooks & configurations."
  (setf tab-width 2)
  (setf go-ts-mode-indent-offset 2)
  (add-hook 'before-save-hook 'lsp-organize-imports nil t)
  (add-hook 'before-save-hook 'lsp-format-buffer nil t)
  (add-hook 'before-save-hook 'delete-trailing-whitespace nil t))

(use-package go-ts-mode
  :mode "\\.go\\'"
  :hook ((go-mode . setup-go-mode)
         (go-mode . lsp)
         (go-ts-mode . setup-go-mode)
         (go-ts-mode . lsp)))

(use-package flycheck-golangci-lint
  :ensure t
  :config
  (flycheck-golangci-lint-setup)
)

(provide '_go)
;;; _go.el ends here
