;;; _elm.el --- Elm specific configurations

;; This file is not part of GNU Emacs
;;; Commentary:
;;; Code:


(use-package elm-mode
  :hook ((elm-mode . lsp)
         (elm-mode . elm-format-on-save-mode))
  :bind ("C-c C-f" . #'elm-format-buffer))


(provide '_elm)
;;; _elm.el ends here
