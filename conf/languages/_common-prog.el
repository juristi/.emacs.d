;; _common-prog.el --- Configure and enable general development packages.
;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:
(require 'use-package)

;; Set sane default
(customize-set-variable 'standard-indent 2)

(global-flycheck-mode)
;; use eslint with web-mode for jsx files
(flycheck-add-mode 'javascript-eslint 'web-mode)

(add-hook 'prog-mode-hook #'electric-pair-local-mode)

(require 'company)
(add-hook 'prog-mode-hook #'company-mode-on)
(add-hook 'comint-mode-hook #'company-mode-on)

;; Language Server Protocol mode


(use-package lsp-mode
  :commands lsp
  :init (setq lsp-keymap-prefix "C-c l")
  :custom
  (lsp-diagnostics-provider :auto "Prefer flycheck when checking source code")
  :bind (:map lsp-mode-map
              ([remap xref-find-apropos] . lsp-ivy-workspace-symbol))
  :hook ((lsp-mode . lsp-lens-mode)
         (lsp-mode . lsp-ui-mode)))

(use-package lsp-ivy)
(use-package lsp-ui
  :bind (:map lsp-ui-mode-map
              ("C-c l !" . lsp-ui-flycheck-list)))

;; Debug Adapter Protocol mode
(require 'dap-elixir)
(use-package dap-mode
  :hook ((dap-mode . dap-ui-mode)))

;; Custom commands / Javascript & Typescript
(defun custom/find-local-eslint ()
  "Use the project local eslint from npm_modules if it exists."
  (when (executable-find "npm")
    (let ((eslint-path)
          (local-eslint-found-p)
          (npm-output (split-string (shell-command-to-string "npm bin") "[\r\n]" t)))
      (while (and npm-output (not local-eslint-found-p))
        (let ((fname (expand-file-name "eslint" (car npm-output))))
          (when (file-executable-p fname)
            (setq eslint-path fname)
            (setq local-eslint-found-p t)))
        (setq npm-output (cdr npm-output)))
      eslint-path)
    ))

(provide '_common-prog)

;;; _common-prog.el ends here
