;; _typescript.el --- Typescript specific packages and configurations

;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:
(defun setup-typescript-mode ()
  "Setup Javascript Typescript env."
  (interactive)
  (when (or (string-suffix-p ".ts" buffer-file-name)
            (string-suffix-p ".tsx" buffer-file-name)
            (string-suffix-p ".js" buffer-file-name)
            (string-suffix-p ".jsx" buffer-file-name))
    (lsp)
    (electric-pair-local-mode)
    (prettier-mode)
    (add-hook 'before-save-hook #'lsp-organize-imports -10 t)))

(use-package web-mode
  :mode "\\.jsx\\'"
  :hook ((web-mode . setup-typescript-mode))
  :custom
  (typescript-indent-level 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2))

(use-package tsx-ts-mode
  :ensure nil
  :mode "\\.tsx\\'"
  :hook ((tsx-ts-mode . setup-typescript-mode))
  :custom
  (typescript-indent-level 2)
  (typescript-ts-mode-indent-offset 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 2)
  (web-mode-code-indent-offset 2))

(use-package typescript-ts-mode
  :ensure nil
  :mode "\\.ts\\'"
  :hook ((typescript-ts-mode . setup-typescript-mode))
  :custom
  (typescript-indent-level 2)
  (typescript-ts-mode-indent-offset 2))

(provide '_typescript)

;;; _typescript.el ends here
