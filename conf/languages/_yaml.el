;;; _yaml.el --- YAML specific configurations

;; This file is not part of GNU Emacs
;;; Commentary:
;;; Code:

(use-package yaml-ts-mode
  :ensure nil
  :mode ("\\.yml\\'" "\\.yaml\\'")
  :hook ((yaml-ts-mode . lsp)))

(provide '_yaml)
;;; _yaml.el ends here
