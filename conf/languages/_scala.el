;;; _scala.el --- Scala specific configurations

;; This file is not part of GNU Emacs
;;; Commentary:
;;; Code:

(require 'use-package)

;; Enable scala-mode for highlighting, indentation and motion commands
(use-package scala-mode
  :interpreter ("scala" . scala-mode)
  :hook ((scala-mode . lsp-mode)
         (scala-mode . lsp)))

;; Enable sbt mode for executing sbt commands
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; ;; allows using SPACE when in the minibuffer
  ;; (substitute-key-definition
  ;;  'minibuffer-complete-word
  ;;  'self-insert-command
  ;;  minibuffer-local-completion-map)
  ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
  (setq sbt:program-options '("-Dsbt.supershell=false"))
)

;; Add metals backend for lsp-mode
(use-package lsp-metals
  :after (lsp-mode)
  :config (setq lsp-metals-treeview-show-when-views-received t))

(provide '_scala)
;;; _scala.el ends here
