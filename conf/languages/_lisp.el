;; _lisp.el --- Lisp specific configurations

;; This file is not part of GNU Emacs

;;; Commentary:

;;; Code:
;;; -*- lexical-binding: t -*-

;; Better handling of paranthesis when writing Lisps.
(use-package paredit
  :ensure t
  :hook ((clojure-mode . enable-paredit-mode)
         (cider-repl-mode . enable-paredit-mode)
         (emacs-lisp-mode . enable-paredit-mode)
         (eval-expression-minibuffer-setup . electric-pair-local-mode)
         (ielm-mode . enable-paredit-mode)
         (lisp-mode . enable-paredit-mode)
         (lisp-interaction-mode . enable-paredit-mode)
         (scheme-mode . enable-paredit-mode)
         (sly-mode . enable-paredit-mode)
         (paredit-mode . (lambda () (electric-indent-local-mode -1))))
  :config
  (show-paren-mode t)
  :diminish nil)

(use-package ielm
  :defer t
  :bind (:map paredit-mode-map
              ("RET" . nil)
              ("C-j" . paredit-newline)))

(use-package sly
  :custom
  (sly-net-coding-system 'utf-8-unix "Use UTF-8 encoding in Sly"))

(use-package sly-mrepl
  :ensure sly
  :after sly
  :bind (:map paredit-mode-map
              ("RET" . nil)
              ("C-j" . paredit-newline)
         :map sly-mrepl-mode-map
         ("<C-up>" . sly-mrepl-previous-input-or-button)
         ("<C-down>" . sly-mrepl-next-input-or-button))
  )

(let ((lispexe (or (executable-find "sbcl")
                   (executable-find "ccl64"))))
  (when lispexe (setf inferior-lisp-program lispexe)))

;; (slime-setup '(slime-fancy slime-company))
;; (setf sly-net-coding-system 'utf-8-unix)

(provide '_lisp)

;;; _lisp.el ends here
