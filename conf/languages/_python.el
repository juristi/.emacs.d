;;; _python.el --- Python specific configurations

;; This file is not part of GNU Emacs
;;; Commentary:
;;; Code:

(elpy-enable)

(setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
(add-hook 'elpy-mode-hook 'flycheck-mode)
(add-hook 'elpy-mode-hook
          (lambda ()
            (add-hook 'before-save-hook #'elpy-black-fix-code nil t)))

(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "--simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters
             "jupyter")

(add-hook 'python-mode-hook #'lsp)

; Enable electric pair mode in Python shell
(add-hook 'inferior-python-mode-hook #'electric-pair-local-mode)

; Set Flycheck's Flake8 config file name to the one used by the Flake8
; project.
(setq flycheck-flake8rc ".flake8")

; Initialize Python Poetry specific items only when the executable has been installed.

(when (and (executable-find "poetry")
           (package-installed-p 'poetry))
  (require 'poetry)
  (add-hook 'elpy-mode-hook 'poetry-tracking-mode)
  (projectile-register-project-type 'python-poetry '("pyproject.toml" "poetry.lock")
                                    :project-file "pyproject.toml"
                                    :compile "poetry build"
                                    :test "poetry run python -m pytest"
                                    :test-prefix "test_"
                                    :test-suffix"_test"))

(provide '_python)
;;; _python.el ends here
