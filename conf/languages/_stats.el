;;; _stats.el --- Emancs speaks statistics related packages and configuration

;; This file is not part of GNU Emacs
;;; Commentary:
;;; Code:

(use-package ess
  :ensure t)

(provide '_stats)
;;; _stats.el ends here
