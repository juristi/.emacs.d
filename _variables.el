;;; _variables.el --- Custom variables for init -*- lexical-binding: t -*-

;; This file is not part of GNU Emacs

;;; Commentary:
;; This file introduces the configration variables used during the
;; Emacs config initialization process.  The actual values should be
;; stored in the local-settings.el and left out of the version control
;; system.

;;; Code:

(defvar elixir-ls-path nil "Define Elixir language server location if not in path.")

(defvar juristi/org-directory
  "~/doc/org/"
  "Root directory for the org files.")

(defvar juristi/org-roam-directory
  (concat juristi/org-directory "roam/")
  "Root directory for the org-roam notes.")
(defvar juristi/org-agenda-work-project-directory
  (concat juristi/org-directory "work/")
  "Root directory for the work project files.")
(defvar juristi/org-caldav-directory
  (concat juristi/org-directory "cal/")
  "Root directory for the org-caldav calendar files.")
(defvar juristi/org-caldav-state-directory
  (concat juristi/org-caldav-directory "state/")
  "Root directory for the org-caldav calendar files.")
(defvar juristi/org-caldav-url nil "URL for the org-caldav calendars.")
(defvar juristi/org-caldav-calendars nil "Org-caldav calendars.")
(defvar juristi/org-caldav-calendar-files nil "Org-caldav calendar files to be added to agenda files.")

(defvar juristi/org-agenda-directory juristi/org-directory)
(defvar juristi/org-agenda-files
  nil
  "Agenda files.")
(defvar juristi/org-refile-targets
  juristi/org-agenda-files
  "Task refile target files.")

(provide '_variables)
;;; _variables.el ends here
